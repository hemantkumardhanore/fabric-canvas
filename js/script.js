$(document).ready(function(){
    var canvas1  = new fabric.Canvas('c1');
	var canvas2  = new fabric.Canvas('c2');
	
	var object1 = new fabric.Circle({
		radius: 30,
		fill: 'black',
		left: 100,
		top: 100
	});
	canvas1.add(object1); 
	var object2 = new fabric.Rect({
		width: 50,
		height: 50,
		fill: 'black',
		opacity: 1,
		left: 10,
		top: 10
	});
	canvas1.add(object2); 
	
	canvas1.on('mouse:down', function() {
        if(this.getActiveObject()) {
            activeObject  = $.extend({}, this.getActiveObject());
            initialCanvas = this.lowerCanvasEl.id;
        }
    });
	canvas2.on('mouse:down', function() {
        if(this.getActiveObject()) {
            activeObject  = $.extend({}, this.getActiveObject());
            initialCanvas = this.lowerCanvasEl.id;
        }
    });
	$(document).on('mouseup', function(evt) {
        if(evt.target.localName === 'canvas' && initialCanvas) {
            canvasId = $(evt.target).siblings().attr('id');
			var posX = evt.clientX;
			var posY = evt.clientY;
			canvasWidth = canvas1.width;
			objectHeight = activeObject.height;
			objectHeight = objectHeight/2;
			objectWidth = activeObject.width;
			objectWidth = objectWidth/2;
            if(canvasId !== initialCanvas) {
				activeObject.setTop(posY-objectHeight);
				if(canvasId === 'c2'){
					activeObject.setLeft((posX - canvasWidth)-objectWidth);
					canvas2.add(activeObject);
					canvas2.renderAll();
					canvas1.getActiveObject().remove();
				}
                else{	
					activeObject.setLeft(posX-objectWidth);				
					canvas1.add(activeObject);
					canvas1.renderAll();
					canvas2.getActiveObject().remove();
				}
            }
        }
        initialCanvas = '';
        activeObject  = {};                 
    }); 
		
});
